# Client Chat SCC4
# Desenvolvedor: Arthur Busqueiro

## Criar uma API com Spring para consumir os seguintes serviços:

-- Inverter Lista
** GET http://localhost:8080/listaReversa?lista={25,35,40,21}

-- Imprimir Ímpares
** GET http://localhost:8080/imprimirImpares?lista={25,35,40,21}

-- Imprimir Pares
** GET http://localhost:8080/imprimirPares?lista={25,35,40,21}

-- Imprimir Tamanho da Palavra
** GET http://localhost:8080/tamanho?palavra=desafio

-- Imprimir Todas as letras maiusculas
** GET http://localhost:8080/maiusculas?palavra=desafio

-- Imprimir Todas as vogais
** GET http://localhost:8080/vogais?palavra=desafio

-- Imprimir Todas as vogais
** GET http://localhost:8080/consoantes?palavra=desafio

-- Imprimir nome em formato bibliográfico
** GET http://localhost:8080/nomeBibliografico?nome=Paulo%pereira%junior



### Para rodar a API, use o Spring Boot Maven ou Plugins Gradle para criar um .jar executável
### Após gerar o .jar, rode o seguinte comando:

> java -jar target/client-chat-0.0.1-SNAPSHOT.jar


## Criar uma aplicação em Angular para consumir a API

### A aplicação deve possuir as seguintes telas:

-- Cadastro de Remetente

-- Cadastro de Remetentes por CSV

-- Envio de mensagens para destinatários


### Para rodar a aplicação, tenha instalado o NodeJS
### Abra o terminal de comando e digite:

> npm install -g @angular/cli

### Acesse a pasta do projeto pelo terminal e digite:

> npm install

### Este comando instalará as dependências

## Rodando a aplicação:

> ng serve

### A aplicação estará rodando na porta 4200

> http://localhost:4200