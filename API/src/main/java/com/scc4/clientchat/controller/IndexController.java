/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scc4.clientchat.controller;

import com.scc4.clientchat.Criptografia;
import com.scc4.clientchat.model.Remetente;
import com.scc4.clientchat.repository.RemetenteRepository;
import com.scc4.clientchat.security.AccountCredentials;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author arthu
 */
@RestController
@RequestMapping
public class IndexController {

    @Autowired
    private RemetenteRepository rementeRepository;

    @GetMapping("/listaReversa")
    public String listaReversa(@RequestParam String lista) {
        List<Integer> copy = stringToList(lista);
        Collections.reverse(copy);
        return listToString(copy);
    }

    @GetMapping("/imprimirImpares")
    public String listarImpares(@RequestParam String lista) {
        List<Integer> copy = stringToList(lista);
        List<Integer> odds = new ArrayList<>();

        for (int si : copy) {
            if (si % 2 != 0) {
                odds.add(si);
            }
        }
        return listToString(odds);
    }

    @GetMapping("/imprimirPares")
    public String listarPares(@RequestParam String lista) {
        List<Integer> copy = stringToList(lista);
        List<Integer> evens = new ArrayList<>();

        for (int si : copy) {
            if (si % 2 == 0) {
                evens.add(si);
            }
        }
        return listToString(evens);
    }

    @GetMapping("/tamanho")
    public String tamanhoPalavra(@RequestParam String palavra) {
        return "tamanho=" + palavra.length();
    }

    @GetMapping("/maiusculas")
    public String maiusculasPalavra(@RequestParam String palavra) {
        return palavra.toUpperCase();
    }

    @GetMapping("/vogais")
    public String vogaisPalavra(@RequestParam String palavra) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < palavra.length(); i++) {
            if (isVowel(palavra.charAt(i))) {
                sb.append(palavra.charAt(i));
            }
        }
        return sb.toString();
    }

    @GetMapping("/consoantes")
    public String consoantesPalavra(@RequestParam String palavra) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < palavra.length(); i++) {
            if (isConsonant(palavra.charAt(i))) {
                sb.append(palavra.charAt(i));
            }
        }
        return sb.toString();
    }

    @GetMapping("/nomeBibliografico")
    public String nomeBibliografico(@RequestParam String nome) {
        StringBuilder sb = new StringBuilder();
        String[] parts = nome.split("%");
        sb.append(parts[parts.length - 1].toUpperCase());
        sb.append(", ");
        sb.append(parts[0].substring(0, 1).toUpperCase());
        sb.append(parts[0].substring(1).toLowerCase());
        if (parts.length > 3) {
            for (int i = 1; i < parts.length - 2; i++) {
                sb.append(" ");
                sb.append(parts[i].substring(0, 1).toUpperCase());
            }
        }
        sb.append(" ");
        sb.append(parts[parts.length - 2].substring(0, 1).toUpperCase());
        sb.append(parts[parts.length - 2].substring(1).toLowerCase());

        return sb.toString();
    }

    @GetMapping("/saque")
    public String saque(@RequestParam int valor) {
        if (valor < 8){
            return "Valor para saque R$" + valor + " não diponível.\n"
                    + "Digite um vaor maior que R$8";
        }
        int total5 = 0;
        int total3 = 0;
        int total = 0;
        int saque = valor;
        while (saque > 0 && total < valor) {
            if (saque % 5 == 0) {
                total5 = saque / 5;
                saque = 0;
            } else {
                saque -= 3;
                total3++;
            }
            total = (5 * total5) + (3 * total3);
        }
        if (total != valor) {
            return "Valor para saque R$" + valor + " não disponível. Digite um valor múltiplo de 3 e 5.\n"
                    + "Valor mais próximo disponível: R$" + total;
        }
        return "Saque R$" + valor + ": "
                + (total3 > 0
                        ? ((total3 + (total3 > 1
                                ? " notas "
                                : " nota ")
                        + "de R$3"))
                        : "")
                + (total3 > 0 && total5 > 0 ? " e " : "")
                + (total5 > 0
                        ? (total5 + (total5
                        > 1
                                ? " notas "
                                : " nota ")
                        + "de R$5")
                        : "");
    }

    private List<Integer> stringToList(String string) {
        string = string.replaceAll("[{ }]", "");
        String[] values = string.split(",");
        List<Integer> lista = new ArrayList<Integer>();
        for (int i = 0; i < values.length; i++) {
            lista.add(Integer.parseInt(values[i]));
        }
        return lista;
    }

    private String listToString(List<Integer> list) {
        StringBuilder retorno = new StringBuilder();
        retorno.append("{");
        for (int i = 0; i < list.size(); i++) {
            retorno.append(list.get(i));
            if (i != list.size() - 1) {
                retorno.append(",");
            }
        }
        retorno.append("}");
        return retorno.toString();
    }

    private boolean isVowel(char c) {
        switch (c) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                return true;
            default:
                return false;
        }
    }

    private boolean isConsonant(char c) {
        switch (c) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                return false;
            default:
                return true;
        }
    }
}
