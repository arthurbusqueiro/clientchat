/*
 *
 *  Copyright (c) 2018-2020 Givantha Kalansuriya, This source is a part of
 *   Staxrt - sample application source code.
 *   http://staxrt.com
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */
package com.scc4.clientchat.controller;

import com.scc4.clientchat.Criptografia;
import com.scc4.clientchat.exception.ResourceNotFoundException;
import com.scc4.clientchat.security.AccountCredentials;
import com.scc4.clientchat.model.Remetente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.scc4.clientchat.repository.RemetenteRepository;
import com.scc4.clientchat.security.MessageBody;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import org.springframework.web.multipart.MultipartFile;

/**
 * The type User controller.
 *
 * @author Givantha Kalansuriya
 */
@RestController
@RequestMapping("/remetentes")
public class RemetenteController {

    @Autowired
    private RemetenteRepository rementeRepository;

    /**
     * Get all rementes list.
     *
     * @return the list
     */
    @GetMapping
    public List<Remetente> listRemetentes() {
        return rementeRepository.findAll();
    }

    /**
     * Create remente remente.
     *
     * @param remente the remente
     * @return the remente
     */
    @PostMapping
    public ResponseEntity<Remetente> create(@Valid @RequestBody Remetente remetente) {
        Remetente exist = rementeRepository.findByEmail(remetente.getEmail());
        if (exist == null){
            Criptografia cripto = new Criptografia();
            remetente.setSenha(cripto.criptografaSenhaMD5(remetente.getSenha()));
            return ResponseEntity.ok(rementeRepository.save(remetente));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<Remetente> login(@Valid @RequestBody AccountCredentials login) {
        Criptografia cripto = new Criptografia();
        login.setSenha(cripto.criptografaSenhaMD5(login.getSenha()));
        Remetente rs = rementeRepository.login(login.getEmail(), login.getSenha());
        return rs != null ? ResponseEntity.ok(rs) : ResponseEntity.badRequest().body(null);
    }

    @PostMapping("/message")
    public boolean sendMessage(@Valid @RequestBody MessageBody body) {
        Remetente remetente = rementeRepository.getOne((long) body.getRemetente());
        System.out.println(remetente.getName() + " enviou a seguinte mensagem:\n" + body.getMensagem());
        for (int i = 0; i < body.getDestinatarios().length; i++) {
            Remetente r = rementeRepository.getOne((long) body.getDestinatarios()[i]);
            System.out.println(remetente.getName() + " enviou esta mensagem para " + r.getName());
        }
        return true;
    }

    @PostMapping("/uploadCSV")
    public ResponseEntity<List<Remetente>> uploadCSV(@RequestParam MultipartFile file) throws IOException {
        byte[] bytes = file.getBytes();
        String completeData = new String(bytes);
        String[] rows = completeData.split("\r\n");
        String[] columns = rows[0].split(",");
        Criptografia cripto = new Criptografia();
        List<Remetente> lista = new ArrayList<Remetente>();
        for (int i = 1; i < rows.length; i++) {
            String[] row = rows[i].split(",");
            Remetente r = new Remetente();
            r.setSenha(cripto.criptografaSenhaMD5("123456"));
            for (int j = 0; j < columns.length; j++) {
                switch (columns[j]) {
                    case "name":
                        r.setName(row[j]);
                        break;
                    case "email":
                        r.setEmail(row[j]);
                        break;
                    case "endereco":
                        r.setEndereco(row[j]);
                        break;
                    case "cpf":
                        r.setCPF(row[j]);
                        break;
                    case "phone":
                        r.setPhone(row[j]);
                        break;
                    default:
                        break;
                }
            }
            Remetente exist = rementeRepository.findByEmail(r.getEmail());
            if (exist != null){
                r = rementeRepository.save(r);
                lista.add(r);
            }
        }
        return lista.size() > 0 ? ResponseEntity.ok(lista) : ResponseEntity.badRequest().body(lista);
    }
}
