/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scc4.clientchat.security;

/**
 *
 * @author arthu
 */
public class MessageBody {
    private int[] destinatarios;
    private String mensagem;
    private int remetente;

    /**
     * @return the destinatarios
     */
    public int[] getDestinatarios() {
        return destinatarios;
    }

    /**
     * @param destinatarios the destinatarios to set
     */
    public void setDestinatarios(int[] destinatarios) {
        this.destinatarios = destinatarios;
    }

    /**
     * @return the mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * @param mensagem the mensagem to set
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * @return the remetente
     */
    public int getRemetente() {
        return remetente;
    }

    /**
     * @param remetente the remetente to set
     */
    public void setRemetente(int remetente) {
        this.remetente = remetente;
    }
}
