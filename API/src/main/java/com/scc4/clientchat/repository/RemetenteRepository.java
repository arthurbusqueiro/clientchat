package com.scc4.clientchat.repository;

import com.scc4.clientchat.model.Remetente;
import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * The interface User repository.
 *
 * @author Givantha Kalansuriya
 */
@Repository
public interface RemetenteRepository extends JpaRepository<Remetente, Long> {
    @Query("FROM Remetente WHERE email = ?1 AND senha = ?2")
    Remetente login(String email, String senha);
    @Query("FROM Remetente WHERE email = ?1")
    Remetente findByEmail(String email);
}
