import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';

import { AppCookieService } from './app-cookie.service';
import { DOCUMENT } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private _httpClient: HttpClient,
    private _appCookieService: AppCookieService,
    @Inject('environments') private environments: any,
  ) {
  }

  formatDate(date: any, datetime: boolean = false) {
    const _date = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    };
    const _dateTime = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    };

    const options = datetime ? _dateTime : _date;
    const newDate = new Date(date);

    return newDate.toLocaleString('pt-BR', options);
  }

  deleteAllCookie() {
    this._appCookieService.deleteAllCookies();
  }

  // Controle dos cookies de autenticação*****************************************
  getCookieAuth() {
    return this._appCookieService.getCookie(this.environments.cookie_auth);
  }
  setCookieAuth(value) {
    this._appCookieService.setCookie(value, this.environments.cookie_auth);
  }
  deleteCookieAuth() {
    this._appCookieService.deleteCookie(this.environments.cookie_auth);
  }
  // ******************************************************************************


  // Retorna os dados do usuário logado no cookie**********************************
  getUserData() {
    const cookieVal = this.getCookieAuth();
    if (cookieVal) {
      return cookieVal;
    }
    return false;
  }

  // Retorna o token do usuário no cookie*******************************************
  getUserToken() {
    const cookieVal = this.getUserData();
    if (cookieVal && Object(cookieVal.senha)) {
      return cookieVal.senha;
    }
  }

  // Cria um Eemetente*************************************************
  register(body) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.post(this.environments.url_api + 'remetentes', body, { headers });
  }

  // Cria um Remetente*************************************************
  login(body) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.post(this.environments.url_api + 'remetentes/login', body, { headers });
  }

  // Busca os Remetentes*************************************************
  findRemetentes() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.get(this.environments.url_api + 'remetentes', { headers });
  }

  // Busca os Remetentes*************************************************
  sendMessage(body) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');

    return this._httpClient.post(this.environments.url_api + 'remetentes/message', body, { headers });
  }

  // Busca os Remetentes*************************************************
  uploadCSV(body) {
    const headers = new HttpHeaders();

    return this._httpClient.post(this.environments.url_api + 'remetentes/uploadCSV', body, { headers });
  }

}
