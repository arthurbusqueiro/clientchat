import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRouting } from './app.routing';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DefaultComponent } from './layouts/default/default.component';
import { CleanComponent } from './layouts/clean/clean.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MessageComponent } from './pages/message/message.component';
import { AuthGuard } from './services/auth.guard';
import { environment } from '../environments/environment';
import { AppCookieService } from './services/app-cookie.service';
import { BroadcastEventService } from './services/broadcast-event.service';
import { CommonService } from './services/common.service';
import { BrowserCookiesModule } from '@ngx-utils/cookies/browser';
import { GooglePlacesDirective } from './directives/google-places.directive';
import { MessageService } from 'primeng/components/common/messageservice';
import { ToastModule } from 'primeng/toast';
import {MultiSelectModule} from 'primeng/multiselect';
import { RegisterManyComponent } from './pages/register-many/register-many.component';
import {TableModule} from 'primeng/table';
import {InputMaskModule} from 'primeng/inputmask';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    DefaultComponent,
    CleanComponent,
    PageNotFoundComponent,
    NavbarComponent,
    MessageComponent,
    GooglePlacesDirective,
    RegisterManyComponent
  ],
  imports: [
    BrowserCookiesModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastModule,
    MultiSelectModule,
    TableModule,
    InputMaskModule,
    AppRouting
  ],
  providers: [
    AuthGuard,
    { provide: 'environments', useValue: environment },
    AppCookieService,
    BroadcastEventService,
    MessageService,
    CommonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
