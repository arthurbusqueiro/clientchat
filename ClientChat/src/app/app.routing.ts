import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layouts/default/default.component';
import { HomeComponent } from './pages/home/home.component';
import { AuthGuard } from './services/auth.guard';
import { CleanComponent } from './layouts/clean/clean.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { MessageComponent } from './pages/message/message.component';
import { RegisterManyComponent } from './pages/register-many/register-many.component';


const AppRoutes: Routes = [
  {// Layout CleanComponent
    path: '',
    component: DefaultComponent,
    children: [
      {
        path: '',
        component: MessageComponent,
        canActivate: [AuthGuard]
      },
    ]
  },
  {// Layout CleanComponent
    path: '',
    component: CleanComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'register',
        component: RegisterManyComponent
      },
    ]
  },
  {// Layout NotFoundComponent
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(AppRoutes, {})],
  exports: [RouterModule]
})
export class AppRouting { }
