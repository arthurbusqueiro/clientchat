import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { AppToastsService } from 'src/app/services/app-toast.service';
import { Router } from '@angular/router';

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {

  @Output() changeMethod: EventEmitter<string> = new EventEmitter();

  public addressText = '';
  public registerForm = new FormGroup({
    endereco: new FormControl(
      { value: null, disabled: false }, Validators.compose([Validators.required])
    ),
    cpf: new FormControl(
      { value: null, disabled: false }, Validators.compose([Validators.required])
    ),
    name: new FormControl(
      { value: '', disabled: false }, Validators.compose([Validators.required])
    ),
    email: new FormControl(
      { value: '', disabled: false }, Validators.compose([Validators.required, Validators.pattern(EMAIL_REGEX)])
    ),
    phone: new FormControl(
      { value: '', disabled: false }, Validators.compose([Validators.required])
    ),
    senha: new FormControl(
      { value: '', disabled: false }, Validators.compose([Validators.required, Validators.minLength(6)])
    ),
    conf_senha: new FormControl(
      { value: '', disabled: false }, Validators.compose([Validators.required, Validators.minLength(6)])
    )
  });

  constructor(
    private _commonService: CommonService,
    private _appToastsService: AppToastsService,
    private _router: Router
  ) { }

  ngOnInit() {
  }

  submitRegisterForm() {
    if (this.registerForm.get('conf_senha').value !== this.registerForm.get('senha').value) {
      this.registerForm.get('senha').setErrors({ mismatch: true });
      this.registerForm.get('conf_senha').setErrors({ mismatch: true });
    }
    if (this.registerForm.valid) {
      this._commonService.register(this.registerForm.value).subscribe(
        (response: any) => {
          this._commonService.setCookieAuth(response);
          this._appToastsService.addToast('success', 'Sucesso', 'Cadastro realizado com sucesso');
          this._router.navigate(['/']);
        },
        err => {
          console.log(err);
          this._appToastsService.addToast('error', 'Erro', 'Este e-mail já está sendo utilizado.');
        }
      );
    }
  }

  goToLogin() {
    this.changeMethod.emit('login');
  }

  goToRegister() {
    this._router.navigate(['register']);
  }

  setAddress(evt) {
    this.registerForm.get('endereco').setValue(evt.address_formatted);
  }

}
