import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  private remetente: any;
  constructor(
    private _commonService: CommonService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.remetente = this._commonService.getUserData();
  }

  signout() {
    this._commonService.deleteAllCookie();
    this._router.navigate(['/home']);
  }
}
