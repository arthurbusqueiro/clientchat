import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppToastsService } from 'src/app/services/app-toast.service';

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  @Output() changeMethod: EventEmitter<string> = new EventEmitter();

  public loginForm = new FormGroup({
    email: new FormControl(
      { value: '', disabled: false }, Validators.compose([Validators.required, Validators.pattern(EMAIL_REGEX)])
    ),
    senha: new FormControl(
      { value: '', disabled: false }, Validators.compose([Validators.required, Validators.minLength(6)])
    )
  });

  constructor(
    private _commonService: CommonService,
    private _router: Router,
    private _toastService: AppToastsService
  ) { }

  ngOnInit() {
  }

  submitLoginForm() {
    if (this.loginForm.valid) {
      this._commonService.login(this.loginForm.value).subscribe(
        (response: any) => {
          this._commonService.setCookieAuth(response);
          this._router.navigate(['/']);
        },
        err => {
          console.log(err);
          this._toastService.addToast('warn', 'Alerta', 'E-mail ou senha inválido.');
        }
      );
    }
  }

  register() {
    this.changeMethod.emit('register');
  }
}