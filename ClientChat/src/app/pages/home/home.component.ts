import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  private method = 'login';
  constructor(
    private _commonService: CommonService
  ) { }

  ngOnInit() {

  }

  changeMethod(method){
    console.log(method);
    this.method = method;
  }

}
