import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';
import { AppToastsService } from 'src/app/services/app-toast.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.less']
})
export class MessageComponent implements OnInit {

  public messageForm = new FormGroup({
    destinatarios: new FormControl(
      { value: null, disabled: false }, Validators.compose([Validators.required])
    ),
    mensagem: new FormControl(
      { value: null, disabled: false }, Validators.compose([Validators.required])
    ),
    remetente: new FormControl(
      { value: null, disabled: false }, Validators.compose([Validators.required])
    ),
  });
  remetentes: any[];
  userLogged: any;
  constructor(
    private _commonService: CommonService,
    private _toastService: AppToastsService
  ) { }

  ngOnInit() {
    this.userLogged = this._commonService.getUserData();
    this.messageForm.get('remetente').setValue(this.userLogged.id);
    this.findRemetentes();
  }

  findRemetentes() {
    this._commonService.findRemetentes().subscribe(
      (response: any) => {
        this.remetentes = [];
        response.forEach(element => {
          if (this.userLogged.id !== element.id) {
            this.remetentes.push({
              label: element.name,
              value: element.id
            });
          }
        });
      },
      err => {
        console.log(err);
        this._toastService.addToast('error', 'Erro', 'Problema ao trazer remetentes');
      }
    );
  }

  submit() {
    this._commonService.sendMessage(this.messageForm.value).subscribe(
      response => {
        this._toastService.addToast('success', 'Sucesso', 'Mensagem enviada com sucesso.');
        this.messageForm.get('destinatarios').setValue(null);
        this.messageForm.get('mensagem').setValue(null);
      }
    );
  }

}
