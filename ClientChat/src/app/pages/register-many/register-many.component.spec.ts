import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterManyComponent } from './register-many.component';

describe('RegisterManyComponent', () => {
  let component: RegisterManyComponent;
  let fixture: ComponentFixture<RegisterManyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterManyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterManyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
