import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { AppToastsService } from 'src/app/services/app-toast.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-many',
  templateUrl: './register-many.component.html',
  styleUrls: ['./register-many.component.less']
})
export class RegisterManyComponent implements OnInit {

  cols = [
    {field: 'name', header: 'Nome'},
    {field: 'cpf', header: 'CPF'},
    {field: 'email', header: 'E-mail'},
    {field: 'endereco', header: 'Endereço'},
  ];
  remetentes: any[];
  constructor(
    private _commonService: CommonService,
    private _toastService: AppToastsService,
    private _router: Router
  ) { }

  ngOnInit() {
  }

  submit(evt) {
    const frmData = new FormData();

    frmData.append('file', evt.target.files[0]);
    this._commonService.uploadCSV(frmData).subscribe(
      (response: any) => {
        this._toastService.addToast('success', 'Sucesso', 'Remetentes incluidos com sucesso.');
        this.remetentes = response;
      },
      err => {
        this._toastService.addToast('warn', 'Alerta', 'Nenhum remetente incluído.');
        this._router.navigate(['/home']);
      }
    );
  }
}
