export const environment = {
  production: true,
  domain_cookie: 'localhost',
  url_api: 'http://localhost:8080/',
  cookie_auth: 'auth'
};
